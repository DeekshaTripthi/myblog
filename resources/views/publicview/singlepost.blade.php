@extends('layouts.public') 
@push('css')
 <link rel="stylesheet" type="text/css" href="{{asset('../css/singlepost.css')}}">
@endpush
@section('content') 

 
     <main class="blog-post-single">
        <div class="container">
            <h1 class="post-title wow fadeInUp">Category - {{$postData->title}}</h1>
            <div class="row">
                <div class="col-md-8 blog-post-wrapper">
                    <div class="post-header wow fadeInUp">
                        <img src="{{asset('storage/'.$postData->thumbnail)}}" alt="blog post" class="post-featured-image">
                        <h3>{{$postData->post_title}}</h3>
                        <p class="post-date">{{date('F d, Y', strtotime($postData->created_at))}}</p>
                    </div>
                    <div class="post-content wow fadeInUp">
                       {!!$postData->post_content!!}
                    </div>
                    <div class="post-tags wow fadeInUp">
                        <a href="#!" class="post-tag">Design Studio</a>
                        <a href="#!" class="post-tag">Creative Design</a>
                    </div>
                    <div class="post-navigation wow fadeInUp">
                        <button class="btn prev-post"> Prev Post</button>
                        <button class="btn next-post"> Next Post</button>
                    </div>
                    <div class="comment-section wow fadeInUp">
                        <h5 class="section-title">Leave a Reply</h5>
                        <form action="POST" class="oleez-comment-form">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="oleez-input" id="fullName" name="fullName" required>
                                    <label for="fullName">*Full name</label>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" class="oleez-input" id="fullName" name="email" required>
                                    <label for="email">*Email</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="message">*Message</label>
                                    <textarea name="message" id="message" rows="10" class="oleez-textarea" required></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-submit">Send</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @include('layouts.sidebar')
            </div>
        </div>
    </main>
@endsection	     
