@extends('layouts.public')
@section('content')
    <main class="blog-standard">
        <div class="container">
            <h1 class="oleez-page-title wow fadeInUp">{{ $cate_title ?? 'Latest Post' }}</h1>
            <div class="row">

                    <div class="col-md-8">
                        @foreach($allPosts as $post)
                            <article class="blog-post wow fadeInUp">
                            <a href="{{ route('single-post', $post->slug) }}">
                              <img src="{{ asset('storage/'.$post->thumbnail)}}" alt="blog post" class="post-thumbnail">
                            </a>  
                            
                            <p class="post-date">{{date('F d, Y', strtotime($post->created_at))}}</p>
                            <a href="{{ route('single-post', $post->slug) }}" style="color:black;text-decoration: none"><h4 class="post-title">{{$post->post_title}}</h4></a>
                            
                            <p class="post-excerpt">{!!substr($post->post_content, 0, 300)!!}</p>
                            <a href="{{ route('single-post', $post->slug) }}" class="btn btn primaryb readMore"> Read more &rarr;</a>
                        </article>

                        @endforeach

                        {{ $allPosts->links('vendor.pagination.default') }}
                      
                        <!--  <nav class="oleez-pagination wow fadeInUp">
                            @for($i=1;$i<=$allPosts->lastPage();$i++)
                              <a href="{{$allPosts->url($i)}}">{{$i}}</a>
                            @endfor
                            <a href="{{$allPosts->nextPageUrl()}}" class="next">&rarr;</a>
                        </nav>  -->

                        
                    </div>
                    @include('layouts.sidebar')
                </div>
        </div>
    </main>
  
  
@endsection    
