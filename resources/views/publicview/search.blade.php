@extends('layouts.public')
@section('content')
    <main class="blog-standard">
        <div class="container">
            <h1 class="oleez-page-title wow fadeInUp">Blog Standard</h1>
            <div class="row">

                    <div class="col-md-8">
                        @foreach($allPosts as $post)
                            <article class="blog-post wow fadeInUp">
                            <img src="{{ asset('storage/'.$post->thumbnail)}}" alt="blog post" class="post-thumbnail">
                            <p class="post-date">{{date('F d, Y', strtotime($post->created_at))}}</p>
                            <h4 class="post-title">{{$post->post_title}}</h4>
                            <p class="post-excerpt">{!!substr($post->post_content, 0, 300)!!}</p>
                            <a href="{{ route('single-post', $post->slug) }}" class="btn btn primaryb readMore" data-id='{{$post->id}}'> Read more &rarr;</a>
                        </article>

                        @endforeach
                      
                         <nav class="oleez-pagination wow fadeInUp">
                            @for($i=1;$i<=$allPosts->lastPage();$i++)
                              <a href="{{$allPosts->url($i)}}">{{$i}}</a>
                            @endfor
                            <a href="{{$allPosts->nextPageUrl()}}" class="next">&rarr;</a>
                        </nav> 

                        
                    </div>
                    @include('layouts.sidebar')
                </div>
        </div>
    </main>
  
  
@endsection    
