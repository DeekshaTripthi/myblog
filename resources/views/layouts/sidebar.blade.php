<div class="col-md-4">
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Tags</h5>
                            <div class="widget-content">
                                <a href="#!" class="post-tag">Design Studio</a>
                                <a href="#!" class="post-tag">Creative Design</a>
                                <a href="#!" class="post-tag">Marketing</a>
                                <a href="#!" class="post-tag">Typography</a>
                                <a href="#!" class="post-tag">Team</a>
                                <a href="#!" class="post-tag">Project</a>
                            </div>
                        </div>
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Share</h5>
                            <div class="widget-content">
                                <nav class="social-links">
                                    <a href="#!">Fb</a>
                                    <a href="#!">Tw</a>
                                    <a href="#!">In</a>
                                    <a href="#!">Be</a>
                                </nav>
                            </div>
                        </div>
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Gallery</h5>
                            <div class="widget-content">
                                <div class="gallery">
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Categories</h5>
                            <div class="widget-content">
                                <ul class="category-list">
                                  @foreach(allCategory() as $cate)
                                    <li><a href="{{route('category-post', $cate->cat_slug)}}">{{$cate->title}}</a></li>    
                                   @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>