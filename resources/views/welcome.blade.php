@extends('layouts.public')
@section('content')
    <main class="blog-standard">
        <div class="container">
            <h1 class="oleez-page-title wow fadeInUp">Blog Standard</h1>
            <div class="row">
                    <div class="col-md-8">
                        @foreach($allPosts as $post)
                            <article class="blog-post wow fadeInUp">
                            <img src="{{ asset('storage/'.$post->thumbnail)}}" alt="blog post" class="post-thumbnail">
                            <p class="post-date">{{$post->created_at}}</p>
                            <h4 class="post-title">{{$post->post_title}}</h4>
                            <p class="post-excerpt">{!!substr($post->post_content, 0, 300)!!}</p>
                            <!-- <a href="#!" class="post-permalink .readMore" data-id="{{$post->id}}">READ MORE</a> -->
                            <button class="btn btn primaryb readMore " data-id='{{$post->id}}'> Read more &rarr;</button>
                        </article>
                        @endforeach


                        <nav class="oleez-pagination wow fadeInUp">
                            <a href="#!" class="active">01</a>
                            <a href="#!">02</a>
                            <a href="#!">03</a>
                            <a href="#!" class="next">&rarr;</a>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Tags</h5>
                            <div class="widget-content">
                                <a href="#!" class="post-tag">Design Studio</a>
                                <a href="#!" class="post-tag">Creative Design</a>
                                <a href="#!" class="post-tag">Marketing</a>
                                <a href="#!" class="post-tag">Typography</a>
                                <a href="#!" class="post-tag">Team</a>
                                <a href="#!" class="post-tag">Project</a>
                            </div>
                        </div>
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Share</h5>
                            <div class="widget-content">
                                <nav class="social-links">
                                    <a href="#!">Fb</a>
                                    <a href="#!">Tw</a>
                                    <a href="#!">In</a>
                                    <a href="#!">Be</a>
                                </nav>
                            </div>
                        </div>
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Gallery</h5>
                            <div class="widget-content">
                                <div class="gallery">
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                    <a href="assets/images/Blogstandard/Blogstandard_@2x.jpg" class="gallery-grid-item" data-fancybox="widget-gallery">
                                        <img src="assets/images/Blogstandard/Blogstandard_@2x.jpg" alt="gallery item">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-widget wow fadeInUp">
                            <h5 class="widget-title">Categories</h5>
                            <div class="widget-content">
                                <ul class="category-list">
                                 @foreach($categories as $category)
                                    <li><a href="">{{$category->title}}</a></li>    
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </main>
@endsection    

    
@push('js')
   <script type="text/javascript">
        $(document).ready(function(){
            $('.readMore').on('click',function(){
              var id = $(this).data('id');
              var myurl= "{{url('admin/post')}}/"+id;
              //alert(myurl);

              $.ajax({
                       type:'GET',
                       url:myurl,
                       dataType: 'json',
                       data:'_token = <?php echo csrf_token() ?>',
                       success:function(data){
                       console.log(data);
                       $('#posts').html(data.html);
                       
                       
                     }
                   });
              

            });//end of show post
        });
    </script>
@endpush