<!-- @extends('layouts.masteradmin') -->


  @section('content')

  <!-- HEADER -->
  
  <header id="main-header" class="py-2 bg-primary text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1><i class="fas fa-cog"></i> Dashboard</h1>
        </div>
      </div>
    </div>
  </header>
  


  <!-- ACTIONS -->
  <section id="actions" class="py-4 mb-4 bg-light">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addPostModal">
            <i class="fas fa-plus"></i> Add Post
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#addCategoryModal">
            <i class="fas fa-plus"></i> Add Category
          </a>
        </div>
        <div class="col-md-3">
          <a href="#" class="btn btn-warning btn-block" data-toggle="modal" data-target="#addUserModal">
            <i class="fas fa-plus"></i> Add User
          </a>
        </div>
      </div>
    </div>
  </section>




  <!-- POSTS -->
<header id="main-header" class="py-2 bg-primary text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1><i class="fas fa-pencil-alt"></i> Posts</h1>
        </div>
      </div>
    </div>
</header>  

<section>
    <div class="container">
      <div class="row">
        <div class="col-md-9" id="postParent">
          <div class="card" id="postContainer">
            <div class="card-header">
              <h4>Latest Posts</h4>
            </div>
            <table class="table table-striped" id="postTable">
              <thead class="table-dark">
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Category</th>
                  <th>Date</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="latestPosts">
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card text-center bg-primary text-white mb-3">
            <div class="card-body">
              <h3>Posts</h3>
              <!-- Added ID for JS -->
              <h4 class="display-4" id="postCount">
                <i class="fas fa-pencil-alt"></i>
              </h4>
              <a href="{{url('admin/post')}}" class="btn btn-outline-light btn-sm">View</a>
            </div>

          </div>
          <div class="card text-center bg-success text-white mb-3">
            <div class="card-body">
              <h3>Categories</h3>
              <h4 class="display-4">
                <i class="fas fa-folder"></i> 4
              </h4>
              <a href="{{url('admin/categories')}}" class="btn btn-outline-light btn-sm">View</a>
            </div>
          </div>
          <div class="card text-center bg-warning text-white mb-3">
            <div class="card-body">
              <h3>Users</h3>
              <h4 class="display-4">
                <i class="fas fa-users"></i> 4
              </h4>
              <a href="{{url('admin/users')}}" class="btn btn-outline-light btn-sm">View</a>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>

  @endsection

  @push('js')
     <script src="{{asset('../js/app1.js')}}"></script>
  @endpush

  