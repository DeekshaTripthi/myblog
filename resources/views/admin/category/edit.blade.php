<form id="editPostForm" action="" method="POST">
                    @csrf
                    @method('PUT')

                    
                      <div class="form-group">
                       <select class="form-control" name="parent_id">
                        <option value="">Select Parent Category</option>

                        @foreach ($otherCategories as $category)
                          <option value="{{ $category->id }}" @if ($category->id==$parentCategory->parent_id){{'selected="selected"'}} @endif >{{ $category->title}}</option>
                        @endforeach
                      </select>
                    </div>
                      <div class="form-group">
                        <input type="text" name="title" class="form-control" value="" placeholder="Category Name" required>
                      </div>
              
                   
</form>