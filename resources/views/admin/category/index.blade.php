@extends('layouts.masteradmin')

@section('content')
  <!-- HEADER -->
  <header id="main-header" class="py-2 bg-success text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1><i class="fas fa-folder"></i> Categories</h1>
        </div>
      </div>
    </div>
  </header>
  <!-- SEARCH -->
  
  <section id="search" class="py-4 mb-4 bg-light">
    <div class="container">
      <div class="row">
        <div class="col-md-4 ml-auto">
          <form action="{{ route('category.index', $order) }}" method="GET">

            <div class="input-group">
            <input type="text" name='search' class="form-control" placeholder="Search Categories..." value="{{request('search')}}">
            <div class="input-group-append">
               
            <div class="form-group">
                      <button type="submit" class="btn btn-success" id="search">Search</button>
            </div>
            </div>
          </form>
          
        </div>
        </div>
      </div>
    </div>
  </section>

  <!-- CATEGORIES -->
  

<!--  -->
@if (Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Success!</h4>
                <p>{{ Session::get('success') }}</p>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

@if (Session::has('errors'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Error!</h4>
        <p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </p>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif

          <div class="container py-3">

            <!-- edit category  model-->

            <div class="modal" tabindex="-1" role="dialog" id="editCategoryModal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Edit Category</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>

                  <form action="" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="modal-body" id="categorydata">
                         
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                  </form>
              </div>
            </div>

            <!-- DELETE MODAL -->

            <div class="modal" tabindex="-1" role="dialog" id="deleteCategoryModal">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Delete Category</h5>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                            

                              <form action="" method="POST">
                                 @csrf
                                 @method('DELETE')

                                <div class="modal-body">
                                  <div class="form-group">
                                    <p>Are you sure! do you want to delete this category?</p>
                                  </div>
                                </div>

                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                </div>
                              </div>
                              </form>
                          </div>
            </div>

            <!-- End DELETE MODEL -->

           
          <div class="row">
            <div class="col-md-8">

              <div class="card">
                <div class="card-header">
                  <h3>Categories</h3>
                </div>
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">
                          <a href="{{ route('category.index', $order) }}">ID</a>
                          
                        </th>
                        @php
                          $order['order_by'] = 'title';
                        @endphp
                        <th scope="col"><a href="{{ route('category.index', $order) }}">Title</a></th>
                        @php
                          $order['order_by'] = 'parent_id';
                        @endphp
                        <th scope="col"><a href="{{ route('category.index', $order) }}">Parent</a></th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    @foreach ($allcategories as $category)
                    <tbody>
                      <tr>
                        <th scope="row">{{$category->id}}</th>
                        <td>{{$category->title}}</td>
                         <td>{{!empty($category->parent['title'])?$category->parent['title']:''}}</td>
                        <td>
                          <button type="button" class="btn btn-sm btn-primary editcate"  data-id="{{ $category->id }}" data-title="{{ $category->title }}">Edit</button> 
                          
                          <button type="button" class="btn btn-sm btn-danger mr-1 delete-category" data-toggle="modal" data-target="#deleteCategoryModal" data-id="{{ $category->id }}" data-title="{{ $category->title }}">Delete</button>
                        </td>
                      </tr>
                    </tbody>

                   @endforeach 
                     

                </table>
                  <nav class="ml-4">
                        <ul class="pagination">
                          <li class="page-item ">
                            <a href="{{$allcategories->previousPageUrl()}}" class="page-link">Previous</a>
                          </li>
                          @for($i=1;$i<=$allcategories->lastPage();$i++)
                          <li class="page-item active">
                            <a href="{{$allcategories->url($i)}}" class="page-link active">{{$i}}</a>
                          </li>
                          @endfor
                          
                          <li class="page-item">
                            <a href="{{$allcategories->nextPageUrl()}}" class="page-link">Next</a>
                          </li>
                        </ul>
                      </nav>
              </div>
             

            </div>

          
          
          <!-- create Category -->
            <div class="col-md-4">
              <div class="card">
                <div class="card-header">
                  <h3>Create Category</h3>
                </div>

                <div class="card-body">
                  <form action="{{route('category.store')}}" method="POST">
                    @csrf

                    <div class="form-group">
                      <select class="form-control" name="parent_id">
                        <option value="">Select Parent Category</option>

                        @foreach ($allcategories as $category)
                          <option value="{{ $category->id }}">{{ $category->title }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <input type="text" name="title" class="form-control" value="{{ old('title') }}" placeholder="Category title" required>
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          <!-- End of create Category -->  

          
        </div>

  


 @endsection 

 


@push('js')
        
        <script type="text/javascript">
          

          $('.editcate').on('click', function() {
             var id = $(this).data('id');
             var title = $(this).data('title');
             var myurl = "{{ url('admin/category') }}/" + id + '/edit';
             var updateurl="{{ url('admin/category') }}/" + id ;
             
             $.ajax({
                 type:'GET',
                 url:myurl, //Make sure your URL is correct
                 dataType: 'json', //Make sure your returning data type dffine as json
                 data:'_token = <?php echo csrf_token() ?>',
                 success:function(data){
                  $('#categorydata').html(data.html);
                  console.log(data);  
                 $('#editCategoryModal').modal();
                 $('#editCategoryModal form input[name="title"]').val(title);
                 $('#editCategoryModal form').attr('action', updateurl);
                 
               }
             });


          });


          $('.delete-category').on('click', function() {
            var id = $(this).data('id');
            var url = "{{ url('admin/category') }}/" + id;
            $('#deleteCategoryModal form').attr('action', url);
          });


         /* $('#search').click(function(){
            alert($( "#mysearch option:selected" ).val())
          });*/

        </script>
@endpush        

