@extends('layouts.masteradmin')
@push('css')
<style type="text/css">
  .invalid-feedback{
    display: block;
  }
  .alert-danger{
    display:none;
  }
</style>
@endpush
@section('content')

 @if (Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Success!</h4>
                <p>{{ Session::get('success') }}</p>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (Session::has('errors'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Error!</h4>
                <p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </p>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif


  <section id="actions" class="py-4 mb-4 bg-light">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <a href="#" class="btn btn-primary btn-block" data-toggle='modal' data-target="#addPostModal">
            <i class="fas fa-plus"></i> Add Post
          </a>
        </div>
       
        
      </div>
    </div>

  </section>
  <section>
    <div class="container">
         <div class="row">
            <div class="col-md-12">

              <div class="card">
                <div class="card-header">
                  <h3>Categories</h3>
                </div>
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">
                          <a href="{{ route('post.index') }}">ID</a>
                          
                        </th>
                        
                        <th scope="col"><a href="{{ route('post.index') }}">Title</a></th>
                       
                        <th scope="col"><a href="{{ route('post.index') }}">Category</a></th>
                        <th scope="col"><a href="{{ route('post.index') }}">Date</a></th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    @foreach ($allPosts as $post)
                    <tbody>
                      <tr>
                        <th scope="row">{{$post->id}}</th>
                        <td>{{$post->post_title}}</td>
                          
                          <td> 
                            @foreach ($allcategories as $category)
                              @if($category->id==$post->cat_id)
                                {{$category->title}}
                             @endif
                            
                            @endforeach
                          </td>
                          
                        <td>{{$post->created_at}}</td>
                        <td>
                          <button type="button" class="btn btn-sm btn-primary editpost"  data-id="{{ $post->id }}" data-title="{{ $post->post_title }}" data-category="{{ $post->post_title }}" data-content="{{ $post->post_content }}" data-thumbnail="{{ $post->thumbnail }}">Edit</button> 

                          <button type="button" class="btn btn-sm btn-danger mr-1 delete-post"   
                            data-toggle="modal" data-target="#deletePostModal" data-id="{{ $post->id }}" data-title="{{ $post->post_title }}">Delete</button>
                        </td>
                      </tr>
                    </tbody>

                   @endforeach 
                  {{$allPosts->links()}}   

                </table>
                  
              </div>
             

            </div>
       </div>
    </div>
 </section>

 <!-- ADD POST MODAL -->


  <div class="modal fade" id="addPostModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary text-white">
          <h5 class="modal-title">Add Post</h5>
          
          <button class="close" data-dismiss="modal">
            <span>&times;</span>
          </button>
        </div>
          <form id="postForm" enctype="multipart/form-data">
          <div class="modal-body">
           
            @csrf
            
            <div class="form-group">
              <label for="post_title">Title</label>
              <input type="text" name="post_title" class="form-control" id="postTitle">
              
              <div class="alert alert-danger" id="post_title"></div>
            


            </div>
            <div class="form-group">
              <label for="category_title">Category</label>
              <select  name="cat_id" class="form-control" id="postCategory">
                <option value="">Choose One...</option>
                @foreach ($allcategories as $category)
                          <option value="{{ $category->id }}">{{ $category->title }}</option>
                @endforeach
              </select>
              
              <div class="alert alert-danger" id="category_title"></div>
            
            </div>
            <div class="form-group">
              <label for="image">Upload Image</label>
              <div>
                <img class="modal-content-img normal" width="50%" height="200" id="postImg" style="display:none"><br/>
              </div>
              <div class="custom-file">
                <label for="thumbnail" class="custom-file-label">Choose File</label>
                <input type="file" name="thumbnail" class="custom-file-input" id="image">
                
                <small class="form-text text-muted">Max Size 3Mb</small>
                <div class="alert alert-danger" id="thumbnail"></div>

              </div>
              <div class="form-group">
                <label for="post_content">Body</label>
                <textarea name="post_content" class="form-control" id="postBody"></textarea>
              </div>
               
            </div>
        </div>
        <div class="modal-footer">
          <!-- Added id for JS -->
          <button type="button" class="btn btn-primary" id="addPostBtn">Add Post</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<!-- END POST MODAL -->



<!-- DELETE POST MODEL-->
 <div class="modal" tabindex="-1" role="dialog" id="deletePostModal">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Delete Category</h5>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                            

                              <form action="" method="POST">
                                 @csrf
                                 @method('DELETE')

                                <div class="modal-body">
                                  <div class="form-group">
                                    <p>Are you sure! Do you really want to delete this Post?</p>
                                  </div>
                                </div>

                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                </div>
                              </div>
                              </form>
  </div>
                
</div>

<!-- EDIT POST MODAL -->
  <div class="modal fade" id="editPostModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary text-white">
          <h5 class="modal-title">Edit Post</h5>
          
          <button class="close" data-dismiss="modal">
            <span>&times;</span>
          </button>
        </div>
          <form method="post" enctype="multipart/form-data">
             @csrf
            @method('PUT')
<!--           <div class="modal-body" id="postdata"> </div>
 -->          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
      </form> 
    </div>
  </div> 
<!--END EDIT POST MODAL -->


<!--END DELETE POST MODEL-->            


@endsection
@push('js')  
 <script>
  
    CKEDITOR.replace('post_content');

    $(document).ready(function(){
            $('.readMore').on('click',function(){
              var id = $(this).data('id');
              var myurl= "{{url('admin/post')}}/"+id;
              //alert(myurl);

              $.ajax({
                       type:'GET',
                       url:myurl,
                       dataType: 'json',
                       data:'_token = <?php echo csrf_token() ?>',
                       success:function(data){
                       console.log(data);
                       $('#posts').html(data.html);
                       
                       
                     }
                   });
              

            });//end of show post
          $("#image").change(function(){
                $("#postImg").css("display", "block")
                readURL(this,'postImg');
           });

          $('#addPostBtn').on('click',function(){
            for ( instance in CKEDITOR.instances ) {
              CKEDITOR.instances[instance].updateElement();
               }
            var myForm = new FormData($("#postForm")[0]);
            var myurl=  "{{url('admin/post')}}";
            $.ajax({

              type:'POST',
              url:myurl,
              data:myForm,
              dataType:'JSON',
              enctype:"multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,        
              })
            .done(function(data) {
                $('#addPostModal').modal('hide');
                $("#postForm")[0].reset();
                
             })
            .fail(function(xhr,status,error){
                var keys= ['post_title','category_title','thumbnail'];
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                var json = $.parseJSON(xhr.responseText);
                var errors = json.errors;
                var i;
                for (i = 0; i < keys.length; i++) {
                   checkKey(errors, keys[i]);
                }
                //checkKey(errors, 'post_title');

             });

          });  //end of addpost

         

          $('.delete-post').on('click', function() {
            var id = $(this).data('id');
            var url = "{{ url('admin/post') }}/" + id;
            $('#deletePostModal form').attr('action', url);
          });//end of delete


          $('.editpost').on('click',function(){

            var id= $(this).data('id');
            var title = $(this).data('title');
            var category = $(this).data('category');
            var content = $(this).data('content');
            var thumbnail = $(this).data('thumbnail');
            var editurl= "{{url('admin/post')}}/"+id+"/edit";
            var updateurl="{{url('admin/post')}}/"+id;
            var img ="{{asset('storage')}}/"+thumbnail;
            alert(img);
            $.ajax({
                 type:'GET',
                 url:editurl, //Make sure your URL is correct
                 dataType: 'json', //Make sure your returning data type dffine as json
                 data:'_token = <?php echo csrf_token() ?>'
                 
             }).done(function(data){
                $('#postdata').html(data.html);
                $('#editPostModal').modal();
                $('#editPostModal form input[name="post_title"]').val(title);
                $('#editPostModal form textarea[name="post_content1"]').val(content);
                $('#editPostModal form').attr('action', updateurl);
                $('#editPostModal img').attr('src',img);
                CKEDITOR.replace('post_content1'); // ADD THIS
                $("#updateimage").change(function(){
                      readURL(this,'updatepostImg');
                });
              
             })
            .fail(function(xhr,status,error){
               $('#editPostModal').modal('show');
              var errorMessage = xhr.status + ': ' + xhr.statusText;
              console.log(errorMessage);
            });

          });//end of edit post


  /*  ALL FUNCTIONS */   

       function checkKey(errors, key ){
             if (errors.hasOwnProperty(key)) {
                 console.log(errors[key]);
                $("#"+key).show().text(errors[key][0]);
              }else{
                 $("#"+key).hide();
              }
          }//end of checkKey function     

      function readURL(input,id) {
                if (input.files && input.files[0]) {
                  var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#'+id).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        } // function to show image preview
    

    }); //end of main
  </script>
@endpush