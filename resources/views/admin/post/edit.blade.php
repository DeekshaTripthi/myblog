 <form action="" id="editPostForm" enctype="multipart/form-data">
           
            @csrf
            @method('PUT')
            
            <div class="form-group">
              <label for="post_title">Title</label>
              <input type="text" name="post_title" class="form-control" id="postTitle">
            </div>
            <div class="form-group">
              <label for="category_title">Category</label>


              <select  name="cat_id" class="form-control" id="postCategory">
                <option value="">Choose One...</option>
                @foreach ($allCategories as $category)
                          <option value="{{ $category->id }}"  @if ($category->id==$selectedcategory->cat_id){{'selected="selected"'}} @endif >{{ $category->title}}</option>
                @endforeach
              </select>
            
            <div class="form-group">
              <label for="image">Upload Image</label>
              <div>
                <img class="modal-content-img normal" width="50%" height="200" id="updatepostImg"><br/>
              </div>
              
              <div class="custom-file">
                <label for="thumbnail" class="custom-file-label">Choose File</label>
                <input type="file" name="thumbnail" class="custom-file-input" id="updateimage">
                
                <small class="form-text text-muted">Max Size 3Mb</small>
                

              </div>
              <div class="form-group">
                <label for="postContent">Body</label>
                <textarea name="post_content1" id="postContent" class="form-control"></textarea>
              </div>
               
            </div>
        </div>
  
</form>
@push('js')

@endpush