@extends('layouts.adminarea')
@push('css')
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
@endpush

  <!-- Content Wrapper. Contains page content -->
  @section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Category</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Categories</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
     <!-- create Category -->
  <section class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
              <div class="card">
                <div class="card-header">
                  <h3>Create Category</h3>
                </div>

                <div class="card-body">
                  <form action="{{route('category.store')}}" method="POST">
                    @csrf

                    <div class="form-group">
                      <select class="form-control select2" name="parent_id">
                        <option value="">Select Parent Category</option>

                        @foreach ($allcategories as $category)
                          <option value="{{ $category->id }}">{{ $category->title }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <input type="text" name="title" class="form-control" value="{{ old('title') }}" placeholder="Category title" required>
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                  </form>
                </div>
              </div>

            </div>
        </div>
      </div>
  </section>   
            
          <!-- End of create Category -->  
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">DataTable with All Categories</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="category_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Title</th>
                    <th>Parent</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                 
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
      
    </section>
    <!-- /.content -->

    
  </div>


<!-- DELETE MODAL -->

            <div class="modal" tabindex="-1" role="dialog" id="deleteCategoryModal">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title">Delete Category</h5>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                            

                              <form action="" method="POST">
                                 @csrf
                                 @method('DELETE')

                                <div class="modal-body">
                                  <div class="form-group">
                                    <p>Are you sure! do you want to delete this category?</p>
                                  </div>
                                </div>

                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                </div>
                              </div>
                              </form>
                          </div>
            </div>

            <!-- End DELETE MODEL -->

   <div class="modal" tabindex="-1" role="dialog" id="editCategoryModal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Edit Category</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>

                  <form action="" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="modal-body" id="categorydata">
                         
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </div>
                  </form>
              </div>
            </div>          

  @endsection
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- jQuery -->

<!-- DataTables -->
@push('js')
<!-- Select2 -->
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#category_table').DataTable({
       processing: true,
        serverSide: true,
        ajax: '{{ url("admin/allcategory") }}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'parent_id', name: 'parent_id'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });


     //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  });

  

  
  function deleteCate(id){
            var url = "{{ url('admin/category') }}/" + id;
            $('#deleteCategoryModal form').attr('action', url);
  }

  function editCate(id,title){
       
       var myurl = "{{ url('admin/category') }}/" + id + '/edit';
       var updateurl="{{ url('admin/category') }}/" + id ;
       
       $.ajax({
           type:'GET',
           url:myurl, //Make sure your URL is correct
           dataType: 'json', //Make sure your returning data type dffine as json
           data:'_token = <?php echo csrf_token() ?>',
           success:function(data){
            $('#categorydata').html(data.html);
            console.log(data);  
           $('#editCategoryModal').modal();
           $('#editCategoryModal form input[name="title"]').val(title);
           $('#editCategoryModal form').attr('action', updateurl);
           
         }
       });


  }


</script>
@endpush


