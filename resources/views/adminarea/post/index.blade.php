@extends('layouts.adminarea')
@push('css')
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
  @endpush

  <!-- Content Wrapper. Contains page content -->
  @section('content')
  <div class="content-wrapper" id="postEditor">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">DataTable with default features</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="post_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                 
                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<!-- DELETE POST MODEL-->
<div class="modal" tabindex="-1" role="dialog" id="deletePostModal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Delete Category</h5>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      

        <form action="" method="POST">
           @csrf
           @method('DELETE')

          <div class="modal-body">
            <div class="form-group">
              <p>Are you sure! Do you really want to delete this Post?</p>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
          </div>
        </form>
      </div>
   </div>    
</div> 

  @endsection
  <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- jQuery -->

<!-- DataTables -->
@push('js')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#post_table').DataTable({
       processing: true,
        serverSide: true,
        ajax: '{{ url("admin/allposts") }}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'post_title', name: 'post_title'},
            {data: 'cat.title', name: 'cat.title'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
  });
  function editPost(id){
           
            var editurl= "{{url('admin/post')}}/"+id+"/edit";
            //var updateurl="{{url('admin/post')}}/"+id;
            var myurl='{{url("admin/post")}}';
           
            $.ajax({
                         type:'GET',
                         url:editurl, //Make sure your URL is correct
                         data:'_token = <?php echo csrf_token() ?>'
                         
                     })
            .done(function(data){
              $('#postEditor').html(data.html);
              console.log(data.successs);
              //$('#postEditor form').attr('action', updateurl);
              $("#updateimage").change(function(){
                      readURL(this,'updatePostImg');
                });
              
              
             })
            .fail(function(xhr,status,error){
              
              var errorMessage = xhr.status + ': ' + xhr.statusText;
              console.log(errorMessage);
            });
       
  }
 
  function deletepost(id){
            var url = "{{ url('admin/post') }}/" + id;
            $('#deletePostModal form').attr('action', url);
  }

  

  function readURL(input,id) {
                if (input.files && input.files[0]) {
                  var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#'+id).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        } // function to show image preview  

  function checkKey(errors, key ){
             if (errors.hasOwnProperty(key)) {
                 console.log(errors[key]);
                $("#"+key).show().text(errors[key][0]);
              }else{
                 $("#"+key).hide();
              }
          }//end of checkKey function   

</script>
@endpush


