

<link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
<style type="text/css">
  .invalid-feedback{
    display: block;
  }
  .alert-danger{
    display:none;
  }
  .alert-success{
    display:none;
  }

</style>

    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <div class="container-fluid">
       @if (Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Success!</h4>
            <p>{{ Session::get('success') }}</p>

            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if (Session::has('errors'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <h4 class="alert-heading">Error!</h4>
                <p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </p>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Update Post</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Text Editors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
               
                <small>Simple and fast</small>
              </h3>
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">

                <form id="postForm" action="{{ route('post.update',$data->id)}}" enctype="multipart/form-data" method="POST" >
                    <div>
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                          <label for="post_title">Title</label>
                          <input type="text" name="post_title" class="form-control" id="postTitle" value="{{ $data->post_title}}">
                          
                          <div class="alert alert-danger" id="post_title"></div>
                        


                        </div>
                        <div class="form-group">
                          <label for="category_title">Category</label>
                          <select  name="cat_id" class="form-control" id="postCategory">
                            <option value=" {{ $data->cat_id }}">{{ $data->title}}</option>
                            @foreach (allCategory() as $category)
                                      <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                          </select>
                          
                          <div class="alert alert-danger" id="category_title"></div>
                        
                        </div>
                        <div class="form-group">
                          <label for="image">Upload Image</label>
                          <div>
                            <img src="{{asset('storage')}}/{{$data->thumbnail}}" class="modal-content-img normal" width="50%" height="200" id="updatePostImg"><br/>
                          </div>
                          <div class="custom-file">
                            <label for="thumbnail" class="custom-file-label">Choose File</label>
                            <input type="file" name="thumbnail" class="custom-file-input" id="updateimage">
                            
                            <small class="form-text text-muted">Max Size 3Mb</small>
                            <div class="alert alert-danger" id="thumbnail"></div>

                          </div>
                           <div class="mb-3">
                            <textarea class="textarea" id="postContent" name="post_content" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"> {{$data->post_content }}</textarea>
                          </div>
                           
                        </div>
                    </div>
                    <div >
                      <!-- Added id for JS -->

                      <button type="submit" class="btn btn-info" id="updatePost">update Post</button>
                    </div>
              </form>
                
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
 <script>
   $(function () {
    // Summernote
    $('.textarea').summernote()
  })

 </script>