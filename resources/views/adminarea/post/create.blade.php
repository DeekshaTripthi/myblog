@extends('layouts.adminarea')
@push('css')
<link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
<style type="text/css">
  .invalid-feedback{
    display: block;
  }
  .alert-danger{
    display:none;
  }
  .alert-success{
    display:none;
  }

</style>
@endpush
@section('content')


  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="alert alert-success alert-dismissible col-sm-6">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fas fa-check"></i> Success!</h5>
                  Your post is added successfully.
                </div>
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Text Editors</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Text Editors</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Write Your Post Here...
                <small>Simple and fast</small>
              </h3>
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
                <form id="postForm" enctype="multipart/form-data">
                    <div>
                       
                        @csrf
                        
                        <div class="form-group">
                          <label for="post_title">Title</label>
                          <input type="text" name="post_title" class="form-control" id="postTitle">
                          
                          <div class="alert alert-danger" id="post_title"></div>
                        


                        </div>
                        <div class="form-group">
                          <label for="category_title">Category</label>
                          <select  name="cat_id" class="form-control" id="postCategory">
                            <option value="">Choose One...</option>
                            @foreach (allCategory() as $category)
                                      <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                          </select>
                          
                          <div class="alert alert-danger" id="category_title"></div>
                        
                        </div>
                        <div class="form-group">
                          <label for="image">Upload Image</label>
                          <div>
                            <img class="modal-content-img normal" width="50%" height="200" id="postImg" style="display:none"><br/>
                          </div>
                          <div class="custom-file">
                            <label for="thumbnail" class="custom-file-label">Choose File</label>
                            <input type="file" name="thumbnail" class="custom-file-input" id="image">
                            
                            <small class="form-text text-muted">Max Size 3Mb</small>
                            <div class="alert alert-danger" id="thumbnail"></div>

                          </div>
                           <div class="mb-3">
                            <textarea class="textarea" id="postContent" name="post_content" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                          </div>
                           
                        </div>
                    </div>
                    <div >
                      <!-- Added id for JS -->
                      <button type="button" class="btn btn-info" id="addPostBtn">Add Post</button>
                    </div>
              </form>
                
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  

@endsection  

<!-- jQuery -->

<!-- Summernote -->
@push('js')
<script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
  $(document).ready(function(){
     $("#image").change(function(){
                $("#postImg").css("display", "block")
                readURL(this,'postImg');
           });

  function readURL(input,id) {
                if (input.files && input.files[0]) {
                  var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#'+id).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        } // function to show image preview  

  function checkKey(errors, key ){
             if (errors.hasOwnProperty(key)) {
                 console.log(errors[key]);
                $("#"+key).show().text(errors[key][0]);
              }else{
                 $("#"+key).hide();
              }
          }//end of checkKey function        

   $('#addPostBtn').on('click',function(event){
      
            var myForm = new FormData($("#postForm")[0]);
            console.log(myForm);
            var myurl=  "{{url('admin/post/')}}";
            $.ajax({
              type:'POST',
              url:myurl,
              data:myForm,
              dataType:'JSON',
              enctype:"multipart/form-data",
              contentType: false,
              cache: false,
              processData: false,        
              })
            .done(function(data) {
                if(data.msg=='success'){
                  $('.alert-success').css('display','block');
                }
                $("#postForm")[0].reset();
                $("#postImg").css("display", "none");
                $("#postContent").summernote("reset");        
             })
            .fail(function(xhr,status,error){
                var keys= ['post_title','category_title','thumbnail'];
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                var json = $.parseJSON(xhr.responseText);
                var errors = json.errors;
                var i;
                for (i = 0; i < keys.length; i++) {
                   checkKey(errors, keys[i]);
                }
               

             });

          });  //end of addpost 

  });
</script>
@endpush

