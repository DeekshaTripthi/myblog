<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\EventController;




/*Route::get('/welcome', function () {
    return view('welcome1');
})->name('myhome');*/


Route::get('post/{slug}',[HomeController::class,'show'])->name('single-post');
Route::get('/',[HomeController::class,'index']);

Route::get('category-post/{cat_slug}',[HomeController::class,'categoryPost'])->name('category-post');
Route::get('search',[HomeController::class,'search'])->name('search');



Route::get('/test', function () {
    return view('test');
});


Route::get('test1',function(){
	return route('myhome',['sortBy'=>'name']);
});

/*Route::get('forgot-password',function(){
   return view('auth.passwords.reset');
});*/

Route::get('/home', ['middleware' => ['auth','admin'], function () {
    return view('adminarea.index');
}]);


	
/*Route::get('admin_area', ['middleware' => ['auth','admin'], function () {
    return view('adminarea.index');
}]);*/

Route::group( ['prefix' => 'admin',  'middleware' => ['auth','admin'] ], function()
{
    Route:: view('/home',[AdminController::class,'index']);
    Route:: get('/profile',[AdminController::class,'profile']);
    Route::get('/allposts',[PostController::class,'datatable']);
    Route::get('/allcategory',[CategoryController::class,'datatable']);
    Route::resource('category', CategoryController::class);
	Route::resource('post', PostController::class);
	Route::resource('event', EventController::class);
	Route::post('/storeEvent',[EventController::class,'storeEvent']);


	
});

Route::get('forgot_password',function(){
	return view('auth.passwords.reset');
} );

Route::view('admin','admin.index');
