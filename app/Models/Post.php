<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Category;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','category_title', 'post_title','post_content','thumbnail','slug'];
     
    public function cat(){
    	 return $this->hasOne('App\Models\Category','id','cat_id');
    }

	 public function createSlug($title, $id = 0)
	    {
	        // Normalize the title
	        $slug = Str::of($title)->slug('-');


	        // Get any that could possibly be related.
	        // This cuts the queries down by doing it once.
	        $allSlugs = $this->getRelatedSlugs($slug, $id);

	        // If we haven't used it before then we are all good.
	        if (! $allSlugs->contains('slug', $slug)){
	            return $slug;
	        }

	        // Just append numbers like a savage until we find not used.
	        for ($i = 1; $i <= 10; $i++) {
	            $newSlug = $slug.'-'.$i;
	            if (! $allSlugs->contains('slug', $newSlug)) {
	                return $newSlug;
	            }
	        }

	        throw new \Exception('Can not create a unique slug');
	    }

	    protected function getRelatedSlugs($slug, $id = 0)
	    {
	        return Post::select('slug')->where('slug', 'like', $slug.'%')
	            ->where('id', '<>', $id)
	            ->get();
	    }
}
