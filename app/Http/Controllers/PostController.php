<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use View;
use App\Models\Post;
use Auth;
use Storage;
use DataTables;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $query = Category::query();
        $allcategories= $query->with('children')->get();
       

        return view('adminarea.post.index',['allcategories'=>$allcategories]);

    }

    public function datatable(Request $request){

        $query = Post::with('cat')->select('*');
        return Datatables::of($query)
                ->addColumn('action', function ($post) {
                    return '<button type="button" class="btn btn-sm btn-primary editpost" onClick="editPost('.$post->id.')" >Edit</button> 

                          <button type="button" class="btn btn-sm btn-danger mr-1 deletepost"   
                            data-toggle="modal" data-target="#deletePostModal" onClick="deletepost('.$post->id.')" data-id="'.$post->id.'" data-title="'.$post->post_title.'">Delete</button>
                    ';
                })->editColumn('created_at',function($post){
                    return date('F d, Y', strtotime($post->created_at));
                })
                ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminarea.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'post_title' =>'required|string|min:3|max:255',
            'cat_id'=> 'required',
            'post_content' => 'required|string',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
             
        ]);

        $post = new Post;
        $post->user_id = Auth::user()->id;
        $post->post_title = $request->post_title;
        $post->cat_id = $request->cat_id;
        $post->post_content = $request->post_content;
        $post->thumbnail = $request->thumbnail->store('images','public');
        $post->slug = $post->createSlug($request->post_title);
        //dd($post);
        $post->save();
        return response()->json(array('success' => true,'msg'=>'success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postData=Post::find($id);
        $html=view('admin.post.show')->with(['postData'=>$postData])->render();
        return response()->json(array('success' => true, 'html'=>$html));
        //return View::make('admin.post.show', array('postData' => $postData));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $allCategories= Category::with('children')->get();
        $data=Post::join('categories','posts.cat_id','=','categories.id')->where('posts.id','=',$id)->select('posts.*','categories.title')->first();
        //dd($data);

        $returnHTML = view('adminarea.post.edit')->with(['allCategories'=>$allCategories,'data'=>$data])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        $post = Post::find($id);
        //code for remove old file
        if($post->thumbnail != ''  && $request->thumbnail != null){
            $path = storage_path().'/app/public/';
            $file_old = $path.$post->thumbnail;
            if(file_exists($file_old)){
                unlink($file_old);
            }
            $post->thumbnail = $request->thumbnail->store('images','public');
        }

        $post->post_title = $request->post_title;
        $post->post_content = $request->post_content;
        $post->cat_id = $request->cat_id;
        $post->save();
        return redirect()->back()->withSuccess('You have successfully updated Post!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {    
        $post =Post::find($id);
        if($post->thumbnail){
            $file_old = storage_path('app/public/'.$post->thumbnail);
            if(file_exists($file_old)){
                unlink($file_old);
            }
        }
        $post->delete();

        return redirect()->route('post.index')->withSuccess('You have successfully deleted a Post!');        
    }
}