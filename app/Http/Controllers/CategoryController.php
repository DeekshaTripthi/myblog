<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use DataTables;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
         $query = Category::query();
         $allcategories= $query->with(['children','parent'])->get();
         return view('adminarea.category.index',['allcategories'=>$allcategories]);
   /*    //$categories = Category::with(['children','parent'])->whereNull('parent_id')->get();

        $query = Category::query();

         $order_arr = [];

        if($request->has('search')){
            $search = $request->search;
            $query->where('categories.title', 'like', '%' . $search . '%');

            $order_arr['search'] = $search;
        }

        if($request->has('order_by')){
            if ($request->order_by=='parent_id') {

                $query->join('categories as t2','categories.id','=','t2.id');
                 $order_by = 't2.title';
                 $order_q = $request->order;

            }
            else{
                $order_by = $request->order_by;
                $order_q = $request->order;
            }
            
                       $order = $request->order == 'ASC' ? 'DESC' : 'ASC';
            $order_arr['order_by'] = $order_by;
            $order_arr['order'] = $order;
        }else{
            $order_by = 'id';
            $order_q = 'DESC';
            $order_arr['order_by'] = $order_by;
            $order_arr['order'] = 'ASC';
        }

        if($request->order_by){
           
           
        }

     //    dd($order_by, $order_q);
        $query->orderBy($order_by, $order_q);
        

       // $order = ['order_by' => 'id', 'order' => 'asc'];
        $order = $order_arr;
       // dd($query);


       $allcategories= $query->with(['children','parent'])->paginate(5);*/

       //return view('admin.category.index', compact('order', 'allcategories'));
      
    }

    public function datatable(Request $request){
        $query = Category::select('*');
        return Datatables::of($query)
                ->addColumn('action', function ($category) {
                    return '<button type="button" class="btn btn-sm btn-primary editcategory"  onClick="editCate('.$category->id.',\''.$category->title.'\')" >Edit</button> 

                          <button type="button" class="btn btn-sm btn-danger mr-1 deletecategory"   
                            data-toggle="modal" data-target="#deleteCategoryModal" onClick="deleteCate('.$category->id.')">Delete</button>
                    ';
                })
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'title'      => 'required|min:3|max:255|string',
            'parent_id' => 'sometimes|nullable|numeric',
        ]);

      $query=new Category;
      $query->title = $request->title;
      $query->parent_id = $request->parent_id;
      $query->cat_slug = $query->createSlug($request->title);
      $query->save();


      //Category::create($validatedData);

      return redirect()->back()->withSuccess('You have successfully created a Category!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $otherCategories= Category::where('parent_id','=',null)->get();
        $data=Category::find($id);
        //dd($data);
        $returnHTML = view('adminarea.category.edit')->with(['otherCategories'=>$otherCategories,'parentCategory'=>$data])->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validate($request, [
            'parent_id'     =>  'sometimes|nullable|numeric',
            'title'  => 'required|min:3|max:255|string'
        ]);

        
        Category::where('id',$id)->update($validatedData);

        return redirect()->route('category.index')->withSuccess('You have successfully updated a Category!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Category::where('id',$id)->delete();

        return redirect()->route('category.index')->withSuccess('You have successfully deleted a Category!');
    }
}
