<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use Auth;


class HomeController extends Controller
{
   
    public function index(Request $request){
    	$post= Post::query();
    	$category= Category::query();
        $allCat=$category->get();
    	$allPosts =  $post->orderBy('created_at','DESC')->paginate(3);

    	//dd($allPosts);
    	 return view('publicview.welcome',['allPosts'=>$allPosts,'categories'=>$allCat]);
    	
    }	

    public function show($slug)
    {
        $postData = Post::join('categories','posts.cat_id','=','categories.id')->where(['slug' => $slug])->first();
        //$postData=Post::where(['slug' => $slug])->first();

        if(!$postData) abort(404);

        return view('publicview.singlepost')->with(['postData'=>$postData]);
       
    }

    public function categoryPost($cat_slug){
          $cate= Category::where(['cat_slug' => $cat_slug])->first();
          $postData = Post::where(['cat_id' => $cate->id])->paginate(3);

           if(!$postData) abort(404);

        return view('publicview.welcome')->with(['allPosts'=>$postData,'cate_title'=>$cate->title]);
    }

    public function search(Request $request){
       $search=  $request->search;
       $post= Post::query();
       $searchPosts=$post->where('post_title', 'like', '%' . $search . '%')->paginate(3);
       return view('publicview.search')->with('allPosts',$searchPosts);

    }




      
    
}
