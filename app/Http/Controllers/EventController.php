<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $allEvents=Event::all();
        $json = json_encode([
            [
            'title' => 'Meetiasdasdsdang',
            'start' => '2020-11-05',
            'allDay ' => false,
            'backgroundColor' => '#0073b7', //Blue
            'borderColor' => '#0073b7' //Blue
            ]
        ]);

        return view('adminarea.calendar', compact('json','allEvents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->data);

        $event = new Event;
        $event->title=$request->data['title'];
        $event->color=$request->data['color'];
        $event->save();
        return response()->json(array('success' => true,'msg'=>'success'));
    }

     public function storeEvent(Request $request){
       dd($request->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function updateEvent(Request $request,$id){
        dd($request->data);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

   
}
