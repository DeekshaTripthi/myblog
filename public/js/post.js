CKEDITOR.replace('post_content');

    $(document).ready(function(){
      $('.readMore').on('click',function(){
        var id = $(this).data('id');
        var myurl= "{{url('admin/post')}}/"+id;
        //alert(myurl);

        $.ajax({
                 type:'GET',
                 url:myurl,
                 dataType: 'json',
                 data:'_token = <?php echo csrf_token() ?>',
                 success:function(data){
                 console.log(data);
                 $('#posts').html(data.html);
                 
                 
               }
             });
        

      });

    $('#addPostBtn').on('click',function(){
      for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }
      var myForm = new FormData($("#postForm")[0]);
      var myurl=  "{{url('admin/post')}}";
      $.ajax({

        type:'POST',
        url:myurl,
        data:myForm,
        dataType:'JSON',
        enctype:"multipart/form-data",
        contentType: false,
        cache: false,
         processData: false,        
         }).done(function(data) {
          $('#addPostModal').modal('hide');
          $("#postForm")[0].reset();
          alert(data);
       })
      .fail(function(xhr,status,error){
          var keys= ['post_title','category_title','thumbnail'];
          var errorMessage = xhr.status + ': ' + xhr.statusText;
          var json = $.parseJSON(xhr.responseText);
          var errors = json.errors;
          var i;
          for (i = 0; i < keys.length; i++) {
             checkKey(errors, keys[i]);
          }
          //checkKey(errors, 'post_title');

       });

    });  

    function checkKey(errors, key ){
       if (errors.hasOwnProperty(key)) {
           console.log(errors[key]);
          $("#"+key).show().text(errors[key][0]);
        }else{
           $("#"+key).hide();
        }
    }

    });